package com.bike.team.compx241_xercisebike;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bike.team.compx241_xercisebike.ListAdapter.SimpleRecycleAdapter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class MessengerActivity extends AppCompatActivity {
    private static final String TAG = "BT Service";

    private EditText userMessage;
    private ConnectedThread connectedThread;

    private RecyclerView recyclerView;
    private ArrayList<Integer> recyclerItems;


    private interface MessageConstants {
        int MESSAGE_READ = 0;
        int MESSAGE_WRITE = 1;
        int MESSAGE_TOAST = 2;

        // ... (Add other message types here as needed.)
    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg){
            Log.d(TAG, String.valueOf(msg.toString()));
            if(msg.what == 0){
                Integer num = byteArrayToInt((byte[]) msg.obj);
                recyclerItems.add(num);
                recyclerView.getAdapter().notifyDataSetChanged();
                Toast.makeText(MessengerActivity.this, String.valueOf(num), Toast.LENGTH_SHORT).show();

                //updateUI();
            }else{
                //showErrorDialog();
                Toast.makeText(MessengerActivity.this, "Sent message", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messenger);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userMessage = findViewById(R.id.editText);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerItems = new ArrayList<>();

        recyclerItems.add(1);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        SimpleRecycleAdapter simpleRecycleAdapter = new SimpleRecycleAdapter(recyclerItems);
        recyclerView.setAdapter(simpleRecycleAdapter);
        recyclerView.getAdapter().notifyDataSetChanged();

        connectedThread = new ConnectedThread(MainActivity.mSocket);
        connectedThread.start();
    }

    public void bluetoothSendMessage(View view) {
//      //TODO: Check connection and send back to home screen if not connected (make fn)

        Log.d(TAG,"stuff="+userMessage.getText());
        connectedThread.write(intToByteArray(Integer.parseInt(userMessage.getText().toString())));
    }

    private static int byteArrayToInt(byte[] b)
    {
        int value = 0;
        for (int i = 0; i < 4; i++) {
            int shift = (4 - 1 - i) * 8;
            value += (b[i] & 0x000000FF) << shift;
        }
        return value;
    }

    private static byte[] intToByteArray(int a)
    {
        byte[] ret = new byte[4];
        ret[3] = (byte) (a & 0xFF);
        ret[2] = (byte) ((a >> 8) & 0xFF);
        ret[1] = (byte) ((a >> 16) & 0xFF);
        ret[0] = (byte) ((a >> 24) & 0xFF);
        return ret;
    }



    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private byte[] mmBuffer; // mmBuffer store for the stream



        ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams; using temp objects because
            // member streams are final.
            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.e(TAG, "ConnectedThread: Error occurred when creating input stream", e);
            }
            try {
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "ConnectedThread: Error occurred when creating output stream", e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            mmBuffer = new byte[1024];
            int numBytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs.
            while (true) {
                try {
                    // Read from the InputStream.
                    numBytes = mmInStream.read(mmBuffer);
                    // Send the obtained bytes to the UI activity.
                    Message readMsg = mHandler.obtainMessage(
                            MessageConstants.MESSAGE_READ, numBytes, -1, mmBuffer);
                    readMsg.sendToTarget();
                    //Log.d(TAG, mHandler.obtainMessage().getData().toString());
                } catch (IOException e) {
                    Log.d(TAG, "ConnectedThread: Input stream was disconnected", e);
                    break;
                }
            }

            cancel();
        }

        // Call this from the main activity to send data to the remote device.
        void write(byte[] bytes) {
            mmBuffer = new byte[1024];

            try {
                mmOutStream.write(bytes);

                // Share the sent message with the UI activity.
                Message writtenMsg = mHandler.obtainMessage(
                        MessageConstants.MESSAGE_WRITE, -1, -1, mmBuffer);
                writtenMsg.sendToTarget();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when sending data", e);

                // Send a failure message back to the activity.
//                Message writeErrorMsg =
//                        mHandler.obtainMessage(MessageConstants.MESSAGE_TOAST);
//                Bundle bundle = new Bundle();
//                bundle.putString("toast", "Couldn't send data to the other device");
//                writeErrorMsg.setData(bundle);
//                mHandler.sendMessage(writeErrorMsg);
            }
        }

        // Call this method from the main activity to shut down the connection.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the connect socket", e);
            }
        }
    }

}
