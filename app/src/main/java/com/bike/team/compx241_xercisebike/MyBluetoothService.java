//package com.bike.team.compx241_xercisebike;
//
//import android.annotation.SuppressLint;
//import android.bluetooth.BluetoothSocket;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.util.Log;
//import android.widget.Toast;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.nio.ByteBuffer;
//
//public class MyBluetoothService {
//    private static final String TAG = "BT Service";
//    private static Handler mHandler; // handler that gets info from Bluetooth service
//    private ConnectedThread connectedThread;
//
//
//
//    private static byte[] intToByteArray(int a)
//    {
//        byte[] ret = new byte[4];
//        ret[3] = (byte) (a & 0xFF);
//        ret[2] = (byte) ((a >> 8) & 0xFF);
//        ret[1] = (byte) ((a >> 16) & 0xFF);
//        ret[0] = (byte) ((a >> 24) & 0xFF);
//        return ret;
//    }
//
//    MyBluetoothService() {
//        Log.d(TAG, "MyBluetoothService: Created BT service");
//    }
//
//    public void CommunicateOn(BluetoothSocket socket, Handler handler) {
//
//        mHandler = handler;
//
//        connectedThread = new ConnectedThread(socket);
//        connectedThread.start();
//        Log.d(TAG, "MyBluetoothService: Connected and ready to communicate");
//    }
//
//    public boolean connected() {
//        return connectedThread != null;
//    }
//
//    public void Sendmsg(Integer num) {
////        ByteBuffer bb = ByteBuffer.allocate(4);
////        bb.putInt(num);
//        Log.d(TAG,"stuff="+num);
//        connectedThread.write(intToByteArray(num));//bb.array());
//    }
//
//    private class ConnectedThread extends Thread {
//        private final BluetoothSocket mmSocket;
//        private final InputStream mmInStream;
//        private final OutputStream mmOutStream;
//        private byte[] mmBuffer; // mmBuffer store for the stream
//
//
//
//        ConnectedThread(BluetoothSocket socket) {
//            mmSocket = socket;
//            InputStream tmpIn = null;
//            OutputStream tmpOut = null;
//
//            // Get the input and output streams; using temp objects because
//            // member streams are final.
//            try {
//                tmpIn = socket.getInputStream();
//            } catch (IOException e) {
//                Log.e(TAG, "ConnectedThread: Error occurred when creating input stream", e);
//            }
//            try {
//                tmpOut = socket.getOutputStream();
//            } catch (IOException e) {
//                Log.e(TAG, "ConnectedThread: Error occurred when creating output stream", e);
//            }
//
//            mmInStream = tmpIn;
//            mmOutStream = tmpOut;
//        }
//
//        public void run() {
//            mmBuffer = new byte[1024];
//            int numBytes; // bytes returned from read()
//
//            // Keep listening to the InputStream until an exception occurs.
//            while (true) {
//                try {
//                    // Read from the InputStream.
//                    numBytes = mmInStream.read(mmBuffer);
//                    // Send the obtained bytes to the UI activity.
//                    Message readMsg = mHandler.obtainMessage(
//                            MessageConstants.MESSAGE_READ, numBytes, -1, mmBuffer);
//                    readMsg.sendToTarget();
//                    //Log.d(TAG, mHandler.obtainMessage().getData().toString());
//                } catch (IOException e) {
//                    Log.d(TAG, "ConnectedThread: Input stream was disconnected", e);
//                    break;
//                }
//            }
//
//            cancel();
//        }
//
//        // Call this from the main activity to send data to the remote device.
//        void write(byte[] bytes) {
//            mmBuffer = new byte[1024];
//
//            try {
//                mmOutStream.write(bytes);
//
//                // Share the sent message with the UI activity.
//                Message writtenMsg = mHandler.obtainMessage(
//                        MessageConstants.MESSAGE_WRITE, -1, -1, mmBuffer);
//                writtenMsg.sendToTarget();
//            } catch (IOException e) {
//                Log.e(TAG, "Error occurred when sending data", e);
//
//                // Send a failure message back to the activity.
//                Message writeErrorMsg =
//                        mHandler.obtainMessage(MessageConstants.MESSAGE_TOAST);
//                Bundle bundle = new Bundle();
//                bundle.putString("toast", "Couldn't send data to the other device");
//                writeErrorMsg.setData(bundle);
//                mHandler.sendMessage(writeErrorMsg);
//            }
//        }
//
//        // Call this method from the main activity to shut down the connection.
//        public void cancel() {
//            try {
//                mmSocket.close();
//            } catch (IOException e) {
//                Log.e(TAG, "Could not close the connect socket", e);
//            }
//        }
//    }
//}
