package com.bike.team.compx241_xercisebike;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;


public class MainActivity extends AppCompatActivity {
    public final String TAG = "BT Main Activity";
    public static BluetoothSocket mSocket;

    //private final static int REQUEST_ENABLE_BT = 1;

    //private OutputStream outputStream;
    //private InputStream inStream;


    private BluetoothAdapter mBluetoothAdapter;

    //private MyBluetoothService myBluetoothService;
    private AcceptThread acceptThread;
    private ConnectThread connectThread;

    //private Integer position = 0;



    private static final UUID MY_UUID_INSECURE = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        //tryBluetooth();
    }

    public boolean bluetoothCommsOn() {

//        if (myBluetoothService != null) {
//            Log.d(TAG, "bluetoothCommsOn: BT service already exists");
//            return true;
//        }

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        String status;

        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Toast.makeText(this, "Please enable Bluetooth", Toast.LENGTH_LONG).show();
            return false;
        }

        String mydeviceaddress = mBluetoothAdapter.getAddress();
        String mydevicename = mBluetoothAdapter.getName();
        status = mydevicename + " : " + mydeviceaddress;

        Toast.makeText(this, status, Toast.LENGTH_LONG).show();

//        myBluetoothService = new MyBluetoothService();

        return true;
    }

    private void startCommunication(BluetoothSocket socket) {
        mSocket = socket;
        Intent intent = new Intent(this, MessengerActivity.class);
        startActivity(intent);
    }

    public void bluetoothServerOn(View view) {
        if (bluetoothCommsOn()) {
            if (connectThread != null) connectThread.cancel();
            acceptThread = new AcceptThread();
            acceptThread.start();
        }
    }

    public void bluetoothClientOn(View view) {

        if (bluetoothCommsOn()) {

            Set<BluetoothDevice> bondedDevices = mBluetoothAdapter.getBondedDevices();

            BluetoothDevice device = null;

            if (bondedDevices.size() > 0) {

                for (BluetoothDevice tdevice : bondedDevices) {
                    if (tdevice.getName().equals("SM-G389F")) {
                        device = tdevice;
                        break;
                    }
                }
                if (device == null) {
                    Log.e(TAG, "bluetoothClientOn: Failed to find the given paired device");
                    return;
                }
            }
            //TODO: Select device rather than have it hard coded

//        Object[] devices = bondedDevices.toArray();
//        System.out.println(devices.length);
//        BluetoothDevice device = (BluetoothDevice) devices[position];
            if (acceptThread != null) acceptThread.cancel();
            connectThread = new ConnectThread(device);
            connectThread.start();
        }
    }




    private class AcceptThread extends Thread {
        private final BluetoothServerSocket mmServerSocket;

        AcceptThread() {
            // Use a temporary object that is later assigned to mmServerSocket
            // because mmServerSocket is final.
            BluetoothServerSocket tmp = null;
            try {
                // MY_UUID is the app's UUID string, also used by the client code.
                tmp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord( "BTServer", MY_UUID_INSECURE);
            } catch (IOException e) {
                Log.e(TAG, "AcceptThread: BluetoothAdapter's listen() method failed", e);
            }
            mmServerSocket = tmp;
        }

        public void run() {
            Log.d(TAG, "AcceptThread: run start");
            BluetoothSocket socket;
            // Keep listening until exception occurs or a socket is returned.
            while (true) {
                try {
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    Log.e(TAG, "AcceptThread: Server socket's accept() method failed", e);
                    break;
                }

                if (socket != null) {
                    // A connection was accepted. Perform work associated with
                    // the connection in a separate thread.
                    startCommunication(socket);

                    //manageMyConnectedSocket(socket);
                    cancel();
                    break;

//                    try {
//                        mmServerSocket.close();
//                    } catch (IOException e) {
//                        Log.e(TAG, "AcceptThread: Could not close the server socket", e);
//                    }
//                    break;
                }
            }
        }

        // Closes the connect socket and causes the thread to finish.
        void cancel() {
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "AcceptThread: Could not close the server socket", e);
            }
        }
    }

    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        ConnectThread(BluetoothDevice device) {
            // Use a temporary object that is later assigned to mmSocket
            // because mmSocket is final.
            BluetoothSocket tmp = null;
            mmDevice = device;

            try {
                // Get a BluetoothSocket to connect with the given BluetoothDevice.
                // MY_UUID is the app's UUID string, also used in the server code.
                tmp = mmDevice.createRfcommSocketToServiceRecord(MY_UUID_INSECURE);
            } catch (IOException e) {
                Log.e(TAG, "ConnectThread: Socket's createRfcommSocketToServiceRecord() method failed", e);
            }
            mmSocket = tmp;
        }

        public void run() {
            // Cancel discovery because it otherwise slows down the connection.
            mBluetoothAdapter.cancelDiscovery();

            try {
                // Connect to the remote device through the socket. This call blocks
                // until it succeeds or throws an exception.
                Log.d(TAG,"ConnectThread: socket is closed? " + String.valueOf(mmSocket == null));
                mmSocket.connect();
            } catch (IOException connectException) {
                // Unable to connect; close the socket and return.

                Log.e(TAG, "ConnectThread: Socket's create() method failed", connectException);

                try {
                    mmSocket.close();
                } catch (IOException closeException) {
                    Log.e(TAG, "ConnectThread: Could not close the client socket", closeException);
                }
                return;
            }

            // The connection attempt succeeded. Perform work associated with
            // the connection in a separate thread.
            startCommunication(mmSocket);
            //manageMyConnectedSocket(mmSocket);
            //cancel();
            Log.d(TAG, "ConnectThread: Connected!");
        }

        // Closes the client socket and causes the thread to finish.
        public void cancel() {
            Log.d(TAG, "ConnectThread: Closing client socket");
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "ConnectThread: Could not close the client socket", e);
            }
        }
    }



}
