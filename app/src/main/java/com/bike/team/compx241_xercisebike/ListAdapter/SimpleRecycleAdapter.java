package com.bike.team.compx241_xercisebike.ListAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bike.team.compx241_xercisebike.R;

import java.util.ArrayList;
import java.util.List;

public class SimpleRecycleAdapter extends RecyclerView.Adapter<SimpleRecycleAdapter.SimpleViewHolder> {

    List<Integer> dataSource;
    //private Context mContext;
    public SimpleRecycleAdapter(ArrayList<Integer> list) {

        dataSource = list;

        //mContext = context;

        // Gen item
//        dataSource = new ArrayList<>();
//        for (int i = 0; i < 10; i++) {
//            dataSource.add(i);
//        }
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_list_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.textView.setText(String.valueOf((dataSource.get(position))));
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {

        public TextView textView;
        public SimpleViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textView);
        }
    }

}
